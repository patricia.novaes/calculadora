package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
    }

    @Test
    public void testeSomaDoisNumerosInteiros(){
        int resultado = calculadora.soma(1, 2);
        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testeSomaDoisNumerosFlutuantes(){
        Double resultado = calculadora.soma(2.3, 3.4);
        Assertions.assertEquals(5.7, resultado);
    }

    // Faça os metodos de divisão por; Numeros inteiros, numeros negativos, numeros flutuantes e também
    // faça os metodos para multiplicação de numeros inteiros, numeros negativos, numeros flutuantes.

    @Test
    public void testeDivisaoNumerosInteiros(){
        int resultado = calculadora.divide(2, 2);
        Assertions.assertEquals(1, resultado);
    }

    @Test
    public void testeDivisaoNumerosNegativos(){
        int resultado = calculadora.divide(-2, -2);
        Assertions.assertEquals(1, resultado);
    }

    @Test
    public void testeDivisaoNumerosFlutuantes(){
        Double resultado = calculadora.divide(2.1, 2.1);
        Assertions.assertEquals(1.0, resultado);
    }

    @Test
    public void testeMultiplicacaoNumerosInteiros(){
        int resultado = calculadora.multiplica(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testeMultiplicacaoNumerosNegativos(){
        int resultado = calculadora.multiplica(2, -2);
        Assertions.assertEquals(-4, resultado);
    }

    @Test
    public void testeMultiplicacaoNumerosFlutuantes(){
        Double resultado = calculadora.multiplica(2.1, 2.1);
        Assertions.assertEquals(4.41, resultado);
    }


}
