package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {

    public Calculadora() {

    }

    public int soma(int numero1, int numero2) {
        int retorno = 0;
        retorno = numero1 + numero2;
        return retorno;
    }

    public double soma(double numero1, double numero2) {
        Double retorno = numero1 + numero2;
        BigDecimal decimalFormat = new BigDecimal(retorno).setScale(3, RoundingMode.HALF_EVEN);

        return decimalFormat.doubleValue();
    }

    public int multiplica(int numero1, int numero2) {
        int retorno = numero1 * numero2;
        return retorno;
    }

    public double multiplica(double numero1, double numero2) {
        Double retorno = numero1 * numero2;
        BigDecimal decimalFormat = new BigDecimal(retorno).setScale(3, RoundingMode.HALF_EVEN);

        return decimalFormat.doubleValue();
    }

    public int divide(int numero1, int numero2) {
        int retorno = numero1 / numero2;
        return retorno;
    }

    public double divide(double numero1, double numero2) {
        Double retorno = numero1 / numero2;
        BigDecimal decimalFormat = new BigDecimal(retorno).setScale(3, RoundingMode.HALF_EVEN);

        return decimalFormat.doubleValue();
    }
}
